﻿using System;
using System.Collections.Generic;
using CsvLib;
using CsvLib.Attributes;

namespace DebugConsole {
	class Program {
		static void Main(string[] args) {
			var serializer = CsvSerializer.FromStandard(CsvStandard.Excel);

			var data = new List<DummyObject>();
			data.Add(new DummyObject() { Okay = 5, Pimmel = "Spac;ken" });
			data.Add(new DummyObject() { Okay = 35, Pimmel = "Ahahahaha" });
			data.Add(new DummyObject() { Okay = 42, Pimmel = "Lol" });
			serializer.Serialize("data.csv", data);

			Console.ReadLine();
		}
	}

	[CsvRow]
	class DummyObject {
		[CsvColumn(SortOrder = 0)]
		public string Pimmel { get; set; }

		[CsvColumn(SortOrder = 1, DisplayName = "OkayM8")]
		public int Okay { get; set; }
	}
}
