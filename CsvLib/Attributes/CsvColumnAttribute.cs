﻿using System;

namespace CsvLib.Attributes {
	public class CsvColumnAttribute : Attribute {
		public int SortOrder { get; set; }
		public string DisplayName { get; set; }
	}
}
