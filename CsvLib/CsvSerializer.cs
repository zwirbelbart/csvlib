﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using CsvLib.Attributes;

namespace CsvLib
{
	public class CsvSerializer {
		public static CsvSerializer FromStandard(CsvStandard standard) {
			switch (standard) {
				case CsvStandard.Excel:
					return new CsvSerializer(";", Environment.NewLine);
				case CsvStandard.Tab:
					return new CsvSerializer("\t", Environment.NewLine);
				case CsvStandard.Rfc4180:
					return new CsvSerializer(",", "\r\n");
				default:
					return new CsvSerializer();
			}
		}

		public CsvSerializer() : this(",", Environment.NewLine) {}

		public CsvSerializer(string seperator, string newLine, bool header = true, Encoding encoding = null) {
			Seperator = seperator;
			NewLine = newLine;
			Encoding = encoding ?? Encoding.UTF8;
			WriteHeader = header;

			//per default include these
			EscapeSequences = new List<string> {Seperator, NewLine};
		}

		public string Seperator { get; set; }
		public string NewLine { get; set; }
		public List<string> EscapeSequences { get; set; }
		public Encoding Encoding { get; set; }
		public bool WriteHeader { get; set; }

		private static IEnumerable<PropertyInfo> GetCsvColumnProperties(Type type) {
			return type.GetProperties().Where(p => p.GetCustomAttributes(typeof(CsvColumnAttribute), true).Length != 0);
		}

		private List<Tuple<string, string>> GetPropertyMap(IEnumerable<PropertyInfo> properties) {
			var result = new List<Tuple<string, string>>();
			foreach (var propertyInfo in properties) {
				foreach (var attr in propertyInfo.GetCustomAttributes(false)) {
					var displayName = ((CsvColumnAttribute)attr).DisplayName;
					var propertyName = propertyInfo.Name;

					if (displayName != null) {
						//todo: check displayName for validity?

						if (NeedsEscape(displayName))
							displayName = EscapeCsvValue(displayName);
					}

					result.Add(new Tuple<string, string>(propertyName, displayName ?? propertyName));
				}
			}

			return result;
		} 

		public void Serialize<T>(string file, IEnumerable<T> data) {
			var type = typeof(T);

			if (type.GetCustomAttributes(typeof(CsvRowAttribute), false).Length == 0)
				throw new Exception(String.Format("Class '{0}' must be marked with CsvRowAttribute", type));

			var fi = new FileInfo(file);

			//get property list ready
			//todo: implement sortorder
			var properties = GetCsvColumnProperties(type);
			
			if(!properties.Any())
				throw new Exception("There are no properties marked with CsvColumnAttribute.");

			//propertyNames: Tuple(Actual Property Name | DisplayName)
			//DisplayName will be the properties name if it was not set
			var propertyNames = GetPropertyMap(properties);
			
			//write file
			using (var writer = new StreamWriter(fi.FullName, false, Encoding) {NewLine = NewLine}) {
				//write header
				if (WriteHeader) {
					var displayNames = propertyNames.Select(it => it.Item2);
					writer.WriteLine(String.Join(Seperator, displayNames));
				}
				
				//write body
				var columnCount = propertyNames.Count;
				var iw = 0;
				var count = data.Count();
				foreach (var huso in data) {
					iw++;

					var row = new string[columnCount];
					for (var i = 0; i < columnCount; i++) {
						var propertyValue = type.GetProperty(propertyNames[i].Item1).GetValue(huso, null).ToString();
						
						//look for things that need escaping
						if (NeedsEscape(propertyValue))
							propertyValue = EscapeCsvValue(propertyValue);

						row[i] = propertyValue;
					}

					var s = String.Join(Seperator, row);

					//no newline on eof
					if(iw == count)
						writer.Write(s);
					else
						writer.WriteLine(s);
				}
			}
		}

		public IEnumerable<T> Deserialize<T>(string file) {
			var type = typeof(T);
			var fi = new FileInfo(file);
			var result = new List<T>();

			//check file
			if(!fi.Exists)
				throw new Exception("Target file does not exist");

			//get properties
			var properties = GetCsvColumnProperties(type);

			if (!properties.Any())
				throw new Exception("There are no properties marked with CsvColumnAttribute.");

			var expectedColumnCount = properties.Count();

			//read file
			using (var reader = new StreamReader(fi.FullName, Encoding)) {
				while (reader.Peek() > 0) {
					var c = reader.Read();

					//todo: parse this fuck
				}
			}

			return result;
		}

		private bool NeedsEscape(string value) {
			return EscapeSequences.Any(sequence => value.Contains(sequence));
		}

		private string EscapeCsvValue(string value) {
			//(most likely) todo: additional escaping
			return String.Format("\"{0}\"", value);
		}

		private string UnescapeCsvValue(string value) {
			//todo: everything
		}
	}
}
