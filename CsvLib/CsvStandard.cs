﻿namespace CsvLib {
	public enum CsvStandard {
		Rfc4180 = 0,
		Excel = 1,
		Tab = 2
	}
}
